import numpy as np
import matplotlib.pyplot as plt


class movParabolico:

    def __init__(self, alpha, vo, xo, yo, g):
        """
        aplha: angulo de lanzamiento 
        vo: velocidad incial
        xo: posicion incial en x
        yo:  posicion incial en y
        g = gravedad"""
        
        self.alpha = alpha
        self.vo = vo
        self.xo = xo
        self.yo = yo
        self.g = g

    def velocidadx(self):
        """Calcula velocidad en x"""
        return self.vo * np.cos(self.alpha)


    def velocidady(self, t):
        """Calcula velocidad en y para un instante de tiempo t"""
        return self.vo * np.sin(self.alpha) - self.g* t
    
    def tiempomax(self):
        """Calcula el tiempo máximo de vuelo"""
        time = 0 #Debido a que necesitamos la velocidad inicial en y
        return 2 *self.velocidady(time) / self.g
            
    def trayectoria(self, time):
        """Retorna una tupla con las posiciones en x y en y para un intervalo de tiempo"""
        posicionx = self.xo + self.vo* np.cos(self.alpha) * time
        posiciony = self.yo + self.vo * np.sin(self.alpha) * time - (0.5)* self.g * (time**2)
        return posicionx, posiciony
        





