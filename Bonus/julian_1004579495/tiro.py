import numpy as np


class MovimientoProyectil:

    def __init__(self, angulo, velocidad_inicial, altura):
        "Se definen las condiciones de angulo, velocidad,altura y gravedad"
        self.angulo = angulo
        self.velocidad_inicial = velocidad_inicial
        self.altura = altura
        self.g = 9.81 # acceleration due to gravity

    def calculate_time_of_flight(self):
        "El tiempo de vuelo que tiene la particula"

        time_of_flight = ((self.velocidad_inicial * np.sin(np.radians(self.angulo)))+ np.sqrt((self.velocidad_inicial * np.sin(np.radians(self.angulo)))**2+2*self.g*self.altura)) / self.g
        return time_of_flight

    
    def calculate_position(self, time):
        "Calcula la posicion de la particulas"
        x = self.velocidad_inicial * np.cos(np.radians(self.angulo)) * time
        y = self.velocidad_inicial * np.sin(np.radians(self.angulo)) * time - 0.5 * self.g * time ** 2 + self.altura
        return (x, y)

