import matplotlib.pyplot as plt
import numpy as np

class puntual_mass:
    '''
    This class create ojects that move like a puntual mass into a constant gravitational field

    Its values to initialice the object are:

    initial_position: a list with [x0,y0]
    angle: the initial throwing angle from the x axis
    velocity: the magnitude of initial velocity
    gravity: the magnitude of gravity
    '''
    def __init__(self, initial_position:list, angle:float, velocity:float, gravity:float, eps:float=0.01) -> None:
        self.x0 = initial_position[0]
        self.y0 = initial_position[1]
        self.angle = angle
        self.v0 = velocity
        self.g = gravity
        self.eps = eps
        self.final_time = ( self.v0*np.sin(self.angle) + np.sqrt((self.v0*np.sin(self.angle))**2 + 2*self.g*self.y0)) / self.g

    def velocity_x(self):
        '''
        This method compute the x-velocity for all flying time
        return array t,vx
        t: array of time from 0 to final time
        vx: array of velocity along x-axis
        '''
        t = np.arange(0,self.final_time+self.eps,self.eps)
        vx = np.ones(len(t))
        vx = vx * (self.v0*np.cos(self.angle))
        return t,vx
    
    def velocity_y(self):
        '''
        This method compute the y-velocity for all flying time
        return array t,vy
        t: array of time from 0 to final time
        vy: array of velocity along y-axis
        '''
        t = np.arange(0,self.final_time+self.eps,self.eps)
        vy = self.v0*np.sin(self.angle)*np.ones(len(t)) - self.g*t
        return t,vy
    
    def position_x(self):
        '''
        This method compute the x-position for all flying time
        return array t,x
        t: array of time from 0 to final time
        x: array of position on x-axis
        '''
        t = np.arange(0,self.final_time+self.eps,self.eps)
        x = self.x0*np.ones(len(t)) + self.v0*np.cos(self.angle)*t
        return t,x
    
    def position_y(self):
        '''
        This method compute the x-position for all flying time
        return array t,y
        t: array of time from 0 to final time
        y: array of position on y-axis
        '''
        t = np.arange(0,self.final_time+self.eps,self.eps)
        y = self.y0*np.ones(len(t)) + self.v0*np.sin(self.angle)*t - 0.5*self.g*(t**2)
        return t,y
    
    def plot(self,name):
        '''
        This method create a plot with the trajectory of the particle

        it has one input for the name of the file. It save the figure in format .png
        '''
        x,y = self.position_x()[1], self.position_y()[1]
        fig, ax = plt.subplots(1,1,figsize=(8,7))
        ax.set_title('Gráfica de trayectoria',size=15)
        ax.set_ylabel('Posición y (m)')
        ax.set_xlabel(r'''Posición x (m)
                      
$x_0={} \, (m)$   $y_0={} \, (m)$   $V_0={} \, (m/s)$   $\theta_0={} \, (rad)$   Tiempo de vuelo $={} \, (s)$
'''.format(self.x0,self.y0,self.v0,round(self.angle, 4), round(self.final_time, 4)))
        
        ax.grid()
        ax.plot(x,y)
        plt.savefig(name+'.png', dpi=200)
